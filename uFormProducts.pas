unit uFormProducts;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.Grids,
  Vcl.DBGrids, Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.StdCtrls;

type
  TformProducts = class(TForm)
    Panel1: TPanel;
    DataSource: TDataSource;
    ADOQuery: TADOQuery;
    Button1: TButton;
    DBNavigator1: TDBNavigator;
    DBGrid1: TDBGrid;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  formProducts: TformProducts;

implementation

{$R *.dfm}

procedure TformProducts.Button1Click(Sender: TObject);
begin
  formProducts.Close;
end;

procedure TformProducts.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  Release;
  formProducts := NIL;
end;

end.
