object formConsumptionItemEdit: TformConsumptionItemEdit
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1069#1083#1077#1084#1077#1085#1090#1099' '#1088#1072#1089#1093#1086#1076#1072
  ClientHeight = 243
  ClientWidth = 548
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 120
  TextHeight = 16
  object Label1: TLabel
    Left = 0
    Top = 120
    Width = 548
    Height = 16
    Align = alTop
    Caption = #1062#1077#1085#1072
    ExplicitWidth = 30
  end
  object Label2: TLabel
    Left = 0
    Top = 160
    Width = 548
    Height = 16
    Align = alTop
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
    ExplicitWidth = 68
  end
  object Panel1: TPanel
    Left = 0
    Top = 202
    Width = 548
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Button1: TButton
      Left = 398
      Top = 0
      Width = 75
      Height = 41
      Align = alRight
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 473
      Top = 0
      Width = 75
      Height = 41
      Align = alRight
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 8
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 548
    Height = 120
    Align = alTop
    DataSource = DataSource
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'id'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'name'
        Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1088#1086#1076#1091#1082#1090#1072
        Width = 482
        Visible = True
      end>
  end
  object editPrice: TEdit
    Left = 0
    Top = 136
    Width = 548
    Height = 24
    Align = alTop
    NumbersOnly = True
    TabOrder = 2
    Text = '0'
  end
  object editCount: TEdit
    Left = 0
    Top = 176
    Width = 548
    Height = 24
    Align = alTop
    NumbersOnly = True
    TabOrder = 3
    Text = '0'
    ExplicitTop = 172
  end
  object DataSource: TDataSource
    DataSet = ADOQuery
    Left = 288
    Top = 48
  end
  object ADOQuery: TADOQuery
    Connection = formMain.ADOConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT id, name FROM dbo.product; ')
    Left = 168
    Top = 48
  end
end
