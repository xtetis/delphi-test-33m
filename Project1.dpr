program Project1;

uses
  Vcl.Forms,
  uFormMain in 'uFormMain.pas' {formMain},
  uFormClients in 'uFormClients.pas' {formClients},
  uFormAuth in 'uFormAuth.pas' {formAuth},
  uFormProducts in 'uFormProducts.pas' {formProducts},
  uFormConsumption in 'uFormConsumption.pas' {formConsumption},
  uFormConsumptionItemEdit in 'uFormConsumptionItemEdit.pas' {formConsumptionItemEdit},
  uFormConsumptionEdit in 'uFormConsumptionEdit.pas' {formConsumptionEdit};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  // Application.CreateForm(TformAuth, formAuth);
  Application.CreateForm(TformMain, formMain);
  // Application.CreateForm(TformAuth, formAuth);
  // Application.CreateForm(TformClients, formClients);
  Application.Run;

end.
