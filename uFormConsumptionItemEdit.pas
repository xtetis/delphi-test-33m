unit uFormConsumptionItemEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Data.Win.ADODB,
  Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls;

type
  TformConsumptionItemEdit = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    DataSource: TDataSource;
    DBGrid1: TDBGrid;
    ADOQuery: TADOQuery;
    editPrice: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    editCount: TEdit;
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  formConsumptionItemEdit: TformConsumptionItemEdit;

implementation

{$R *.dfm}

procedure TformConsumptionItemEdit.Button2Click(Sender: TObject);
begin
  Tform(Self).Close;
end;

end.
