unit uFormAuth;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, uFormMain,
  Data.DB, Data.Win.ADODB, inifiles;

type
  TformAuth = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    editServer: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    editLogin: TEdit;
    Label3: TLabel;
    editPass: TEdit;
    Label4: TLabel;
    editDb: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  formAuth: TformAuth;
  mssql_server: String;
  mssql_login: String;
  mssql_pass: String;
  mssql_dbname: String;

implementation

{$R *.dfm}

procedure TformAuth.Button1Click(Sender: TObject);
var
  ConnString: String;
begin
  mssql_server := TformAuth(Self).editServer.Text;
  mssql_login := TformAuth(Self).editLogin.Text;
  mssql_pass := TformAuth(Self).editPass.Text;
  mssql_dbname := TformAuth(Self).editDb.Text;
  ConnString := 'Provider=SQLOLEDB.1;Persist Security Info=False;' +
    'User ID=%s;Password=%s;Data Source=%s;Initial Catalog=%s;Use Procedure for Prepare=1;'
    + 'Auto Translate=True;Packet Size=4096;Use Encryption for Data=False;' +
    'Tag with column collation when possible=False';
  formMain.ADOConnection.ConnectionString :=
    Format(ConnString, [mssql_login, mssql_pass, mssql_server, mssql_dbname]);

  try
    formMain.ADOConnection.Connected := True;
  except
    on e: Exception do
    begin
      if formMain.ADOConnection.Errors.Count > 0 then
        ShowMessageFmt('������ ��� �����������: %s',
          [formMain.ADOConnection.Errors[0].Description])
      else
        ShowMessageFmt('����������� ������: %s', [e.Message]);
    end;
  end;

end;

procedure TformAuth.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (NOT formMain.ADOConnection.Connected) then
  begin
    Application.Terminate;
    Application.ProcessMessages;
  end
  else
  begin
    Action := caFree;
  end;
end;

end.
