
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'consumption_item_fk2')
   AND parent_object_id = OBJECT_ID(N'dbo.consumption_item')
)
ALTER TABLE [dbo].[consumption_item] DROP CONSTRAINT [consumption_item_fk2]
GO

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'consumption_item_fk')
   AND parent_object_id = OBJECT_ID(N'dbo.consumption_item')
)
ALTER TABLE [dbo].[consumption_item] DROP CONSTRAINT [consumption_item_fk]
GO


















IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.consumption_fk')
   AND parent_object_id = OBJECT_ID(N'dbo.consumption')
)
ALTER TABLE [dbo].[consumption] DROP CONSTRAINT [consumption_fk]
GO

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'DF__consumpti__creat__1920BF5C')
   AND parent_object_id = OBJECT_ID(N'dbo.consumption')
)
ALTER TABLE [dbo].[consumption] DROP CONSTRAINT [DF__consumpti__creat__1920BF5C]
GO

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'DF__consumpti__proce__182C9B23')
   AND parent_object_id = OBJECT_ID(N'dbo.consumption')
)
ALTER TABLE [dbo].[consumption] DROP CONSTRAINT [DF__consumpti__proce__182C9B23]
GO

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'DF__consumpti__total__173876EA')
   AND parent_object_id = OBJECT_ID(N'dbo.consumption')
)
ALTER TABLE [dbo].[consumption] DROP CONSTRAINT [DF__consumpti__total__173876EA]
GO


























/****** Object:  Table [dbo].[client]    Script Date: 29.05.2020 22:01:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[client]') AND type in (N'U'))
DROP TABLE [dbo].[client]
GO

/****** Object:  Table [dbo].[client]    Script Date: 29.05.2020 22:01:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[client](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Первичный ключ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client', @level2type=N'COLUMN',@level2name=N'id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Имя клиента' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client', @level2type=N'COLUMN',@level2name=N'name'
GO









































/****** Object:  Table [dbo].[product]    Script Date: 29.05.2020 22:01:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[product]') AND type in (N'U'))
DROP TABLE [dbo].[product]
GO

/****** Object:  Table [dbo].[product]    Script Date: 29.05.2020 22:01:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[product](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Первичный ключ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'product', @level2type=N'COLUMN',@level2name=N'id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Название продукта' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'product', @level2type=N'COLUMN',@level2name=N'name'
GO





















/****** Object:  Table [dbo].[consumption]    Script Date: 29.05.2020 22:02:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[consumption]') AND type in (N'U'))
DROP TABLE [dbo].[consumption]
GO

/****** Object:  Table [dbo].[consumption]    Script Date: 29.05.2020 22:02:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[consumption](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_client] [int] NULL,
	[total_sum] [money] NULL,
	[processed] [tinyint] NULL,
	[processed_date] [date] NULL,
	[creation_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[consumption] ADD  DEFAULT ((0)) FOR [total_sum]
GO

ALTER TABLE [dbo].[consumption] ADD  DEFAULT ((0)) FOR [processed]
GO

ALTER TABLE [dbo].[consumption] ADD  DEFAULT (getdate()) FOR [creation_date]
GO

ALTER TABLE [dbo].[consumption]  WITH CHECK ADD  CONSTRAINT [consumption_fk] FOREIGN KEY([id_client])
REFERENCES [dbo].[client] ([id])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[consumption] CHECK CONSTRAINT [consumption_fk]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Первичный ключ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'consumption', @level2type=N'COLUMN',@level2name=N'id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ключ клиента' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'consumption', @level2type=N'COLUMN',@level2name=N'id_client'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Общая сумма' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'consumption', @level2type=N'COLUMN',@level2name=N'total_sum'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Признак проведения расхода' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'consumption', @level2type=N'COLUMN',@level2name=N'processed'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Когда провели расход' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'consumption', @level2type=N'COLUMN',@level2name=N'processed_date'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Дата создания записи' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'consumption', @level2type=N'COLUMN',@level2name=N'creation_date'
GO






















/****** Object:  Table [dbo].[consumption_item]    Script Date: 29.05.2020 22:02:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[consumption_item]') AND type in (N'U'))
DROP TABLE [dbo].[consumption_item]
GO

/****** Object:  Table [dbo].[consumption_item]    Script Date: 29.05.2020 22:02:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[consumption_item](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_consumption] [int] NULL,
	[id_product] [int] NULL,
	[count] [int] NULL,
	[price] [money] NULL,
	[sum] [money] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[consumption_item]  WITH CHECK ADD  CONSTRAINT [consumption_item_fk] FOREIGN KEY([id_product])
REFERENCES [dbo].[product] ([id])
ON UPDATE SET NULL
ON DELETE SET NULL
GO

ALTER TABLE [dbo].[consumption_item] CHECK CONSTRAINT [consumption_item_fk]
GO

ALTER TABLE [dbo].[consumption_item]  WITH CHECK ADD  CONSTRAINT [consumption_item_fk2] FOREIGN KEY([id_consumption])
REFERENCES [dbo].[consumption] ([id])
ON UPDATE SET NULL
ON DELETE SET NULL
GO

ALTER TABLE [dbo].[consumption_item] CHECK CONSTRAINT [consumption_item_fk2]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Первичный ключ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'consumption_item', @level2type=N'COLUMN',@level2name=N'id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID расхода' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'consumption_item', @level2type=N'COLUMN',@level2name=N'id_consumption'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ключ продукта' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'consumption_item', @level2type=N'COLUMN',@level2name=N'id_product'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Количество' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'consumption_item', @level2type=N'COLUMN',@level2name=N'count'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Цена' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'consumption_item', @level2type=N'COLUMN',@level2name=N'price'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Сумма' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'consumption_item', @level2type=N'COLUMN',@level2name=N'sum'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'РЎСЃС‹Р»РєР° РЅР° СЂР°СЃС…РѕРґ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'consumption_item', @level2type=N'CONSTRAINT',@level2name=N'consumption_item_fk2'
GO

