

/****** Object:  Trigger [tr_consumption_processed_date]    Script Date: 29.05.2020 22:04:10 ******/
IF OBJECT_ID ('tr_consumption_processed_date', 'TR') IS NOT NULL 
DROP TRIGGER  [dbo].[tr_consumption_processed_date]
GO

/****** Object:  Trigger [dbo].[tr_consumption_processed_date]    Script Date: 29.05.2020 22:04:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[tr_consumption_processed_date] ON [dbo].[consumption]
WITH EXECUTE AS OWNER
FOR INSERT, UPDATE
AS
DECLARE @id_consumption INT
DECLARE @processed INT
DECLARE @total MONEY
BEGIN
	IF EXISTS ( SELECT 0 FROM INSERTED )
		BEGIN
        	SELECT 
            	@id_consumption = id, 
                @processed = processed
            FROM 
            	INSERTED

			SELECT 
				@total = SUM(count * price)
			FROM 
				dbo.consumption_item 
			WHERE
				id_consumption = @id_consumption
                
        	IF (@processed = 1)
            BEGIN




              UPDATE dbo.consumption
              SET 
				processed_date = CURRENT_TIMESTAMP,
				total_sum = @total
              WHERE 
                id = @id_consumption
            END
            ELSE BEGIN
              UPDATE dbo.consumption
              SET 
				processed_date = NULL,
				total_sum = @total
              WHERE 
                id = @id_consumption
            END;
        END
END
GO

ALTER TABLE [dbo].[consumption] ENABLE TRIGGER [tr_consumption_processed_date]
GO























/****** Object:  Trigger [tr_consumption_item]    Script Date: 29.05.2020 22:04:39 ******/
IF OBJECT_ID ('tr_consumption_item', 'TR') IS NOT NULL  
   
DROP TRIGGER  [dbo].[tr_consumption_item]
GO

/****** Object:  Trigger [dbo].[tr_consumption_item]    Script Date: 29.05.2020 22:04:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[tr_consumption_item]
ON [dbo].[consumption_item]
AFTER INSERT, UPDATE, DELETE
AS
DECLARE @id_consumption_item INT
DECLARE @id_consumption INT
DECLARE @count INT
DECLARE @price MONEY
DECLARE @total MONEY

DECLARE @processed INT
BEGIN

	IF EXISTS ( SELECT 0 FROM DELETED )
	BEGIN
		SELECT 
			@id_consumption  = id_consumption 
		FROM 
			DELETED

		SELECT 
			@processed = c.processed
		FROM
			dbo.consumption c
		WHERE
			c.id = @id_consumption

		IF (@processed = 1)
		BEGIN
			ROLLBACK TRANSACTION
		END
	END




	IF EXISTS ( SELECT 0 FROM INSERTED )
	BEGIN
		SELECT 
			@id_consumption  = id_consumption 
		FROM 
			INSERTED

		SELECT 
			@processed = c.processed
		FROM
			dbo.consumption c
		WHERE
			c.id = @id_consumption

		IF (@processed = 1)
		BEGIN
			ROLLBACK TRANSACTION
		END
	END

	IF EXISTS ( SELECT 0 FROM INSERTED )
		BEGIN
        	SELECT 
            	@id_consumption_item= id, 
                @count= count,
                @price = price,
                @id_consumption  = id_consumption 
            FROM 
            	INSERTED


            UPDATE dbo.consumption_item
            SET sum=@count *  @price
            WHERE id = @id_consumption_item


			SELECT 
				@total = SUM(count * price)
			FROM 
				dbo.consumption_item 
			WHERE
				id_consumption = @id_consumption

              UPDATE dbo.consumption
              SET 
				total_sum = @total
              WHERE 
                id = @id_consumption




        END
		
		

	IF EXISTS ( SELECT 0 FROM DELETED )
		BEGIN
        	SELECT 
            	@id_consumption_item= id, 
                @count= count,
                @price = price,
                @id_consumption  = id_consumption 
            FROM 
            	DELETED



			SELECT 
				@total = SUM(count * price)
			FROM 
				dbo.consumption_item 
			WHERE
				id_consumption = @id_consumption

              UPDATE dbo.consumption
              SET 
				total_sum = @total
              WHERE 
                id = @id_consumption




        END
END
GO

ALTER TABLE [dbo].[consumption_item] ENABLE TRIGGER [tr_consumption_item]
GO

