-- =================================================================

IF EXISTS (SELECT *
  FROM sys.foreign_keys
   WHERE object_id = OBJECT_ID(N'dbo.consumption_fk')
   AND parent_object_id = OBJECT_ID(N'dbo.consumption')
)
ALTER TABLE dbo.consumption
DROP CONSTRAINT consumption_fk
GO


ALTER TABLE dbo.consumption
ADD CONSTRAINT consumption_fk FOREIGN KEY (id_client)
  REFERENCES dbo.client (id)
  ON UPDATE CASCADE
  ON DELETE NO ACTION
GO


/* ================================================================= */



IF EXISTS (SELECT *
  FROM sys.foreign_keys
   WHERE object_id = OBJECT_ID(N'dbo.consumption_item_fk')
   AND parent_object_id = OBJECT_ID(N'dbo.consumption_item')
)
ALTER TABLE dbo.consumption_item
DROP CONSTRAINT consumption_item_fk
GO

ALTER TABLE dbo.consumption_item
ADD CONSTRAINT consumption_item_fk FOREIGN KEY (id_product)
  REFERENCES dbo.product (id)
  ON UPDATE CASCADE
  ON DELETE NO ACTION
GO



-- =================================================================



IF EXISTS (SELECT *
  FROM sys.foreign_keys
   WHERE object_id = OBJECT_ID(N'dbo.consumption_item_fk2')
   AND parent_object_id = OBJECT_ID(N'dbo.consumption_item')
)
ALTER TABLE dbo.consumption_item
DROP CONSTRAINT consumption_item_fk2
GO

ALTER TABLE dbo.consumption_item
ADD CONSTRAINT consumption_item_fk2 FOREIGN KEY (id_consumption)
  REFERENCES dbo.consumption (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
GO

EXEC sp_addextendedproperty 'MS_Description', N'Ссылка на расход', N'schema', N'dbo', N'table', N'consumption_item', N'constraint', N'consumption_item_fk2'
GO





