object formConsumption: TformConsumption
  Left = 0
  Top = 0
  Caption = #1056#1072#1089#1093#1086#1076#1099
  ClientHeight = 464
  ClientWidth = 747
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 423
    Width = 747
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Button1: TButton
      Left = 672
      Top = 0
      Width = 75
      Height = 41
      Align = alRight
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 256
      Top = 0
      Width = 123
      Height = 41
      Align = alLeft
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1101#1083'.'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button6: TButton
      Left = 0
      Top = 0
      Width = 128
      Height = 41
      Align = alLeft
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1101#1083'.'
      TabOrder = 2
      OnClick = Button6Click
    end
    object Button7: TButton
      Left = 128
      Top = 0
      Width = 128
      Height = 41
      Align = alLeft
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1101#1083'.'
      TabOrder = 3
      OnClick = Button7Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 747
    Height = 192
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Panel2'
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 0
      Top = 25
      Width = 747
      Height = 126
      Align = alClient
      DataSource = dsConsumption
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'id'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'id_client'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'name'
          Title.Caption = #1050#1083#1080#1077#1085#1090
          Width = 191
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'total_sum'
          Title.Caption = #1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'processed'
          Title.Caption = #1055#1086#1076#1090#1074#1077#1088#1078#1076#1077#1085#1086
          Width = 93
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'processed_date'
          Title.Caption = #1044#1072#1090#1072' '#1087#1086#1076#1090#1074#1077#1088#1078#1076#1077#1085#1080#1103
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'creation_date'
          Title.Caption = #1057#1086#1079#1076#1072#1085#1086
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 747
      Height = 25
      Align = alTop
      Caption = #1056#1072#1089#1093#1086#1076#1099
      TabOrder = 1
    end
    object Panel6: TPanel
      Left = 0
      Top = 151
      Width = 747
      Height = 41
      Margins.Bottom = 5
      Align = alBottom
      BevelOuter = bvNone
      Padding.Bottom = 9
      TabOrder = 2
      object Button3: TButton
        Left = 256
        Top = 0
        Width = 123
        Height = 32
        Align = alLeft
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1088#1072#1089#1093'.'
        TabOrder = 0
        OnClick = Button3Click
      end
      object Button4: TButton
        Left = 128
        Top = 0
        Width = 128
        Height = 32
        Align = alLeft
        Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1088#1072#1089#1093'.'
        TabOrder = 1
        OnClick = Button4Click
      end
      object Button5: TButton
        Left = 0
        Top = 0
        Width = 128
        Height = 32
        Align = alLeft
        Caption = #1059#1076#1072#1083#1080#1090#1100' '#1088#1072#1089#1093'.'
        TabOrder = 2
        OnClick = Button5Click
      end
      object Button8: TButton
        Left = 379
        Top = 0
        Width = 75
        Height = 32
        Align = alLeft
        Caption = #1055#1077#1095#1072#1090#1100
        TabOrder = 3
        OnClick = Button8Click
      end
      object Button9: TButton
        Left = 454
        Top = 0
        Width = 91
        Height = 32
        Align = alLeft
        Caption = #1055#1086#1076#1090#1074#1077#1088#1076#1080#1090#1100
        TabOrder = 4
        OnClick = Button9Click
      end
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 192
    Width = 747
    Height = 231
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 747
      Height = 25
      Align = alTop
      Caption = #1057#1086#1076#1077#1088#1078#1080#1084#1086#1077' '#1088#1072#1089#1093#1086#1076#1072
      TabOrder = 0
    end
    object DBGrid2: TDBGrid
      Left = 0
      Top = 25
      Width = 747
      Height = 206
      Align = alClient
      DataSource = dsConsumptionItem
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'id'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'id_consumption'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'id_product'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'name'
          Title.Caption = #1055#1088#1086#1076#1091#1082#1090
          Width = 340
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'count'
          Title.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
          Width = 101
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'price'
          Title.Caption = #1062#1077#1085#1072
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'sum'
          Title.Caption = #1057#1091#1084#1084#1072
          Visible = True
        end>
    end
  end
  object dsConsumption: TDataSource
    DataSet = ADOQueryConsumption
    Left = 432
    Top = 40
  end
  object ADOQueryConsumption: TADOQuery
    Connection = formMain.ADOConnection
    CursorType = ctStatic
    AfterScroll = ADOQueryConsumptionAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      #1089'on.*,'
      'cl.name'
      'FROM '
      '  dbo.consumption as '#1089'on'
      'LEFT JOIN'
      #9'dbo.client as cl ON'
      '    cl.id = '#1089'on.id_client')
    Left = 232
    Top = 48
  end
  object dsConsumptionItem: TDataSource
    DataSet = ADOQueryConsumptionItem
    Left = 408
    Top = 217
  end
  object ADOQueryConsumptionItem: TADOQuery
    Connection = formMain.ADOConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT  ci.*, p.name FROM dbo.consumption_item ci LEFT JOIN dbo.' +
        'product p ON p.id = ci.id_product')
    Left = 136
    Top = 217
  end
  object SaveTextFileDialog: TSaveTextFileDialog
    Left = 376
    Top = 312
  end
end
