object formAuth: TformAuth
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1040#1074#1090#1086#1088#1080#1079#1072#1094#1080#1103' (MS SQL Server)'
  ClientHeight = 286
  ClientWidth = 432
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 432
    Height = 223
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 43
      Height = 16
      Caption = #1057#1077#1088#1074#1077#1088
    end
    object Label2: TLabel
      Left = 16
      Top = 60
      Width = 35
      Height = 16
      Caption = #1051#1086#1075#1080#1085
    end
    object Label3: TLabel
      Left = 16
      Top = 112
      Width = 43
      Height = 16
      Caption = #1055#1072#1088#1086#1083#1100
    end
    object Label4: TLabel
      Left = 16
      Top = 163
      Width = 28
      Height = 16
      Caption = #1041#1072#1079#1072
    end
    object editServer: TEdit
      Left = 16
      Top = 30
      Width = 393
      Height = 24
      TabOrder = 0
    end
    object editLogin: TEdit
      Left = 16
      Top = 81
      Width = 393
      Height = 24
      TabOrder = 1
    end
    object editPass: TEdit
      Left = 16
      Top = 134
      Width = 393
      Height = 24
      TabOrder = 2
    end
    object editDb: TEdit
      Left = 16
      Top = 185
      Width = 393
      Height = 24
      TabOrder = 3
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 223
    Width = 432
    Height = 63
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Button1: TButton
      Left = 168
      Top = 14
      Width = 91
      Height = 33
      Caption = #1040#1074#1090#1086#1088#1080#1079#1072#1094#1080#1103
      TabOrder = 0
      OnClick = Button1Click
    end
  end
end
