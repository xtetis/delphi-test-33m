﻿object formConsumptionEdit: TformConsumptionEdit
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1056#1072#1089#1093#1086#1076
  ClientHeight = 324
  ClientWidth = 401
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 120
  TextHeight = 16
  object Пользователь: TLabel
    Left = 8
    Top = 64
    Width = 83
    Height = 16
    Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
  end
  object Panel1: TPanel
    Left = 0
    Top = 289
    Width = 401
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Закрыть: TButton
      Left = 326
      Top = 0
      Width = 75
      Height = 35
      Align = alRight
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 8
      TabOrder = 0
      OnClick = ЗакрытьClick
    end
    object OK: TButton
      Left = 251
      Top = 0
      Width = 75
      Height = 35
      Align = alRight
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 1
    end
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 8
    Width = 385
    Height = 275
    BorderStyle = bsNone
    DataSource = dsClients
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'id'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'name'
        Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
        Width = 369
        Visible = True
      end>
  end
  object dsClients: TDataSource
    DataSet = ADOQuery
    Left = 288
    Top = 56
  end
  object ADOQuery: TADOQuery
    Connection = formMain.ADOConnection
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT id, name FROM dbo.client;')
    Left = 208
    Top = 56
  end
end
