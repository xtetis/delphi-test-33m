unit uFormConsumption;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Data.DB,
  Data.Win.ADODB, Vcl.Grids, Vcl.DBGrids, printers, Vcl.ComCtrls,
  System.Win.ComObj, Vcl.ExtDlgs;

type
  TformConsumption = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    dsConsumption: TDataSource;
    DBGrid1: TDBGrid;
    ADOQueryConsumption: TADOQuery;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    DBGrid2: TDBGrid;
    dsConsumptionItem: TDataSource;
    ADOQueryConsumptionItem: TADOQuery;
    Panel6: TPanel;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    SaveTextFileDialog: TSaveTextFileDialog;
    Button9: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ADOQueryConsumptionAfterScroll(DataSet: TDataSet);
    procedure Button7Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure refreshConsumptionItems();
    procedure refreshConsumptions();
  end;

var
  formConsumption: TformConsumption;

implementation

{$R *.dfm}

uses uFormConsumptionEdit, uFormConsumptionItemEdit;

procedure TformConsumption.refreshConsumptionItems();
var
  selected_consumption_id: integer;
  selected_product_id: integer;
begin
  selected_consumption_id := formConsumption.DBGrid1.Fields[0].AsInteger;
  if selected_consumption_id = 0 then
  begin
    exit;
  end;

  formConsumption.ADOQueryConsumptionItem.Close;
  formConsumption.ADOQueryConsumptionItem.SQL.Clear;
  formConsumption.ADOQueryConsumptionItem.SQL.Text :=
    'SELECT  ci.*, p.name FROM dbo.consumption_item ci ' +
    ' LEFT JOIN dbo.product p ON p.id = ci.id_product WHERE ci.id_consumption = :id_consumption';
  formConsumption.ADOQueryConsumptionItem.Parameters.ParamByName
    ('id_consumption').Value := selected_consumption_id;
  formConsumption.ADOQueryConsumptionItem.ExecSQL;
  formConsumption.ADOQueryConsumptionItem.Active := True;
  formConsumption.ADOQueryConsumptionItem.Prepared := True;



end;

procedure TformConsumption.refreshConsumptions();
begin
  formConsumption.ADOQueryConsumption.Close;
  formConsumption.ADOQueryConsumption.SQL.Clear;
  formConsumption.ADOQueryConsumption.SQL.Text :=
    'SELECT �on.*, cl.name FROM  dbo.consumption as �on ' +
    'LEFT JOIN	dbo.client as cl ON    cl.id = �on.id_client';
  formConsumption.ADOQueryConsumption.ExecSQL;
  formConsumption.ADOQueryConsumption.Active := True;
  formConsumption.ADOQueryConsumption.Prepared := True;
end;

procedure TformConsumption.ADOQueryConsumptionAfterScroll(DataSet: TDataSet);
begin
  formConsumption.refreshConsumptionItems();
end;

procedure TformConsumption.Button1Click(Sender: TObject);
begin
  TForm(Self).Close;
end;

procedure TformConsumption.Button2Click(Sender: TObject);
var
  selected_consumption_id: integer;
  selected_product_id: integer;
begin
  selected_consumption_id := formConsumption.DBGrid1.Fields[0].AsInteger;
  if selected_consumption_id = 0 then
  begin
    exit;
  end;

  formConsumptionItemEdit := TformConsumptionItemEdit.Create(Self);

  formConsumptionItemEdit.ADOQuery.SQL.Clear;
  formConsumptionItemEdit.ADOQuery.SQL.Text :=
    'SELECT id, name FROM dbo.product;';
  formConsumptionItemEdit.ADOQuery.ExecSQL;
  formConsumptionItemEdit.ADOQuery.Active := True;
  formConsumptionItemEdit.ADOQuery.Prepared := True;
  ShowScrollBar(formConsumptionItemEdit.DBGrid1.Handle, SB_HORZ, False);

  Try
    if formConsumptionItemEdit.ShowModal = mrOk THEN
    begin
      selected_product_id := formConsumptionItemEdit.DBGrid1.Fields[0]
        .AsInteger;
      formConsumptionItemEdit.ADOQuery.Close;
      formConsumptionItemEdit.ADOQuery.SQL.Clear;
      formConsumptionItemEdit.ADOQuery.SQL.Add
        ('INSERT INTO  dbo.consumption_item (id_consumption, id_product,' +
        ' count, price) VALUES (:id_consumption, :id_product, :count, :price)');
      formConsumptionItemEdit.ADOQuery.Parameters.ParamByName('id_consumption')
        .Value := selected_consumption_id;
      formConsumptionItemEdit.ADOQuery.Parameters.ParamByName('id_product')
        .Value := selected_product_id;
      formConsumptionItemEdit.ADOQuery.Parameters.ParamByName('count').Value :=
        StrToInt(formConsumptionItemEdit.editCount.Text);
      formConsumptionItemEdit.ADOQuery.Parameters.ParamByName('price').Value :=
        StrToInt(formConsumptionItemEdit.editPrice.Text);
      formConsumptionItemEdit.ADOQuery.ExecSQL;

      formConsumption.refreshConsumptionItems();
      formConsumption.refreshConsumptions();
    end;
  finally
    formConsumptionItemEdit.Free;
  End;
end;

procedure TformConsumption.Button3Click(Sender: TObject);
var
  selected_client_id: integer;
begin
  formConsumptionEdit := TformConsumptionEdit.Create(Self);
  Try
    // formAuthModalInt := Integer(formConsumptionEdit);

    formConsumptionEdit.ADOQuery.SQL.Clear;
    formConsumptionEdit.ADOQuery.SQL.Text :=
      'SELECT id, name FROM dbo.client; ';
    formConsumptionEdit.ADOQuery.ExecSQL;
    formConsumptionEdit.ADOQuery.Active := True;
    formConsumptionEdit.ADOQuery.Prepared := True;
    ShowScrollBar(formConsumptionEdit.DBGrid1.Handle, SB_HORZ, False);

    if formConsumptionEdit.ShowModal = mrOk THEN
    begin
      selected_client_id := formConsumptionEdit.DBGrid1.Fields[0].AsInteger;
      formConsumptionEdit.ADOQuery.Close;
      formConsumptionEdit.ADOQuery.SQL.Clear;
      formConsumptionEdit.ADOQuery.SQL.Add
        ('INSERT INTO  dbo.consumption (id_client) VALUES (' +
        IntToStr(selected_client_id) + ')');
      formConsumptionEdit.ADOQuery.ExecSQL;

      formConsumption.refreshConsumptions()
    end;
  finally
    formConsumptionEdit.Free;
  end;
end;

procedure TformConsumption.Button4Click(Sender: TObject);
var
  selected_client_id: integer;
  selected_consumption_id: integer;
begin
  selected_consumption_id := formConsumption.DBGrid1.Fields[0].AsInteger;
  if selected_consumption_id = 0 then
  begin
    exit;
  end;

  formConsumptionEdit := TformConsumptionEdit.Create(Self);
  Try
    formConsumptionEdit.ADOQuery.ExecSQL;
    formConsumptionEdit.ADOQuery.Active := True;
    formConsumptionEdit.ADOQuery.Prepared := True;
    ShowScrollBar(formConsumptionEdit.DBGrid1.Handle, SB_HORZ, False);

    if formConsumptionEdit.ShowModal = mrOk THEN
    begin
      selected_client_id := formConsumptionEdit.DBGrid1.Fields[0].AsInteger;

      formConsumptionEdit.ADOQuery.Close;
      formConsumptionEdit.ADOQuery.SQL.Clear;
      formConsumptionEdit.ADOQuery.SQL.Add
        ('UPDATE dbo.consumption SET id_client = :id_client WHERE  id = :id');
      formConsumptionEdit.ADOQuery.Parameters.ParamByName('id_client').Value :=
        selected_client_id;
      formConsumptionEdit.ADOQuery.Parameters.ParamByName('id').Value :=
        selected_consumption_id;
      formConsumptionEdit.ADOQuery.ExecSQL;

      formConsumption.refreshConsumptions()
    end;
  finally
    formConsumptionEdit.Free;
  end;
end;

procedure TformConsumption.Button5Click(Sender: TObject);
var
  selected_consumption_id: integer;
begin
  selected_consumption_id := formConsumption.DBGrid1.Fields[0].AsInteger;
  if selected_consumption_id = 0 then
  begin
    exit;
  end;

  formConsumption.ADOQueryConsumption.Close;
  formConsumption.ADOQueryConsumption.SQL.Clear;
  formConsumption.ADOQueryConsumption.SQL.Text :=
    'DELETE FROM dbo.consumption WHERE id = :id ';
  formConsumption.ADOQueryConsumption.Parameters.ParamByName('id').Value :=
    selected_consumption_id;
  formConsumption.ADOQueryConsumption.ExecSQL;

  formConsumption.refreshConsumptions()
end;

procedure TformConsumption.Button6Click(Sender: TObject);
var
  selected_consumption_item_id: integer;
  selected_consumption_id: integer;
begin
  selected_consumption_item_id := formConsumption.DBGrid2.Fields[0].AsInteger;
  if selected_consumption_item_id = 0 then
  begin
    exit;
  end;

  formConsumption.ADOQueryConsumptionItem.Close;
  formConsumption.ADOQueryConsumptionItem.SQL.Clear;
  formConsumption.ADOQueryConsumptionItem.SQL.Text :=
    'DELETE FROM  dbo.consumption_item  WHERE id = :id ';
  formConsumption.ADOQueryConsumptionItem.Parameters.ParamByName('id').Value :=
    selected_consumption_item_id;
  formConsumption.ADOQueryConsumptionItem.ExecSQL;

  formConsumption.refreshConsumptionItems();
  formConsumption.refreshConsumptions();
end;

procedure TformConsumption.Button7Click(Sender: TObject);
var
  selected_consumption_item_id: integer;
  selected_product_id: integer;
  selected_count: integer;
  selected_price: integer;
  selected_consumption_id: integer;
begin
  selected_consumption_item_id := formConsumption.DBGrid2.Fields[0].AsInteger;
  if selected_consumption_item_id = 0 then
  begin
    exit;
  end;

  selected_consumption_id := formConsumption.DBGrid1.Fields[0].AsInteger;
  if selected_consumption_id = 0 then
  begin
    exit;
  end;

  selected_product_id := formConsumption.DBGrid2.Fields[2].AsInteger;
  selected_count := formConsumption.DBGrid2.Fields[4].AsInteger;
  selected_price := formConsumption.DBGrid2.Fields[5].AsInteger;

  formConsumptionItemEdit := TformConsumptionItemEdit.Create(Self);

  formConsumptionItemEdit.ADOQuery.SQL.Clear;
  formConsumptionItemEdit.ADOQuery.SQL.Text :=
    'SELECT id, name FROM dbo.product;';
  formConsumptionItemEdit.ADOQuery.ExecSQL;
  formConsumptionItemEdit.ADOQuery.Active := True;
  formConsumptionItemEdit.ADOQuery.Prepared := True;
  ShowScrollBar(formConsumptionItemEdit.DBGrid1.Handle, SB_HORZ, False);


  formConsumptionItemEdit.editPrice.Text := IntToStr(selected_price);
  formConsumptionItemEdit.editCount.Text := IntToStr(selected_count);

  Try
    if formConsumptionItemEdit.ShowModal = mrOk THEN
    begin
      selected_product_id := formConsumptionItemEdit.DBGrid1.Fields[0]
        .AsInteger;
      formConsumptionItemEdit.ADOQuery.Close;
      formConsumptionItemEdit.ADOQuery.SQL.Clear;
      formConsumptionItemEdit.ADOQuery.SQL.Text :=
        'UPDATE  dbo.consumption_item ' + ' SET ' + 'id_product = :id_product, '
        + 'count = :count, ' + 'price = :price ' + 'WHERE id = :id ';
      formConsumptionItemEdit.ADOQuery.Parameters.ParamByName('id').Value :=
        selected_consumption_item_id;
      formConsumptionItemEdit.ADOQuery.Parameters.ParamByName('id_product')
        .Value := selected_product_id;
      formConsumptionItemEdit.ADOQuery.Parameters.ParamByName('count').Value :=
        StrToInt(formConsumptionItemEdit.editCount.Text);
      formConsumptionItemEdit.ADOQuery.Parameters.ParamByName('price').Value :=
        StrToInt(formConsumptionItemEdit.editPrice.Text);
      formConsumptionItemEdit.ADOQuery.ExecSQL;

      formConsumption.refreshConsumptionItems();
      formConsumption.refreshConsumptions();
    end;
  finally
    formConsumptionItemEdit.Free;
  End;
end;

procedure TformConsumption.Button8Click(Sender: TObject);
var
  i: integer;
  OutLine, f: string;
  sTemp, s: string;
  Strings: TstringList;
begin

  // ��������� ���������� ��������
  SaveTextFileDialog.InitialDir := GetCurrentDir;
  SaveTextFileDialog.Filter := 'Excel file|*.csv';

  // ��������� ���������� �� ���������
  SaveTextFileDialog.DefaultExt := '.csv';

  // ����� ��������� ������ ��� ��������� ��� �������
  SaveTextFileDialog.FilterIndex := 1;
  SaveTextFileDialog.FileName := ChangeFileExt(Application.ExeName, '.csv');
  // ����������� ������ ���������� �����
  if SaveTextFileDialog.Execute then
  begin

    Strings := TstringList.Create;
    try
      s := '';

      for i := 1 to DBGrid2.Columns.Count - 1 do
      begin
        if DBGrid2.Columns.Items[i].Visible then
        begin
          s := s + string(DBGrid2.Columns.Items[i].Title.Caption) + ';';
        end;
      end;
      Strings.Add(s);

      while not ADOQueryConsumptionItem.Eof do
      begin
        s := '';
        OutLine := '';
        OutLine := OutLine + ADOQueryConsumptionItem.Fields[6].AsString + ';';
        OutLine := OutLine + ADOQueryConsumptionItem.Fields[3].AsString + ';';
        OutLine := OutLine + ADOQueryConsumptionItem.Fields[4].AsString + ';';
        OutLine := OutLine + ADOQueryConsumptionItem.Fields[5].AsString + ';';

        Strings.Add(OutLine);
        ADOQueryConsumptionItem.Next;
      end;

    finally
      Strings.SaveToFile(SaveTextFileDialog.FileName);
    end;

  end;

end;

procedure TformConsumption.Button9Click(Sender: TObject);
var
  selected_consumption_id: integer;
begin
  selected_consumption_id := formConsumption.DBGrid1.Fields[0].AsInteger;
  if selected_consumption_id = 0 then
  begin
    exit;
  end;


  formConsumption.ADOQueryConsumption.Close;
  formConsumption.ADOQueryConsumption.SQL.Clear;
  formConsumption.ADOQueryConsumption.SQL.Text :=
    'UPDATE dbo.consumption SET processed = 1 WHERE  id = :id ';
  formConsumption.ADOQueryConsumption.Parameters.ParamByName('id').Value :=
    selected_consumption_id;
  formConsumption.ADOQueryConsumption.ExecSQL;

  formConsumption.refreshConsumptions()

end;

procedure TformConsumption.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  Release;
  formConsumption := NIL;
end;

end.
